const express = require('express')
var pjson = require('./package.json');
console.log(pjson.version);

const app = express()
const PORT = 3003

// set the view engine to ejs
app.set('view engine', 'ejs');

app.get('/',(req,res)=>{
    res.send('chan')
})
app.get('/test',(req,res)=>{
    res.render('index',{
        version:pjson.version
    })
})
app.listen(PORT,()=>{console.log(`listen in ${PORT}`)})